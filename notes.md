# Curso de Django

## 1. Introduccion

### 1.1 Preparacion del entorno de trabajo
* Instalar python directamente de su [web](https://www.python.org/)
* Al momento de la instalacon, se debe chequear la casilla de crear *variables de entorno para python en el sistema*
* Verificar instalacion en la consola con __python --version__
* Instalar virtualenv mediante el comando en consola __pip install virtualenv__
* Verificar instalacion con __virtualenv --version__
* Para poder utilizar virutalenv se requiere posicionarnos dentro de la carpeta *Scripts* y utilizar el comando __activate__ y para salir se utiliza __deactivate__

### 1.2 Creacion del proyecto Platzigram / Tu primer Hola Mundo en Django
* Instalar Django mediante consola con __pip install django__
* Verificar instalacion con el comando __pip freeze__
* Creamos un proyecto con el comando __django-admin startproject platzigram__
* Nos crea un folder con cuatro archivos llamado platzigram y un archivo llamado __manage.py__
  * __init.py__ declara platzigram como un modulo de python
  * __Settings.py__ define todas las configuraciones del proyecto
  * __urls.py__ punto de entrada para todas las peticiones que lleguen a nuestro proyecto con django
  * __wsgi.py__ archivo usado durante el deployment en produccion
  * __manage.py__ nunca se debe tocar, pero interactuaremos con el en todo el desarrollo. Crea una interfaz sobre django-admin
* Con el comando __python manage.py runserver__ podemos correr nuestro proyecto
* En el archivo __urls.py__ creamos una __url__ de nombre *hello_world/* la cual reibe una funcion que le responde(HttpResponse) con un *Hello, world!*

## 2. Vistas

### 2.1 El objeto Request
* Refactorizamos el archivo urls.py para separar la logica de las urls
* Creamos una nueva url *hi*
* Creamos una nueva view *hi* la cual recibe numeros, los ordena y los imprime
* OJO *mport pdb; pdb.set_trace()* sirve para debuggear directamente en la consola

### 2.2 Solucion al reto / Pasando argumentos en la URL
* Se ve la solucion al ejemplo anterior de pasar una lista de numeros por url, ordenarla y mostrarla como un json, se cambian nombres a la URL y la funcion de manera que tienen uno mas acorde a lo que realizan.
* Se crea otra URL __hi__ para ver como django recibe parametros mediante URL de una manera mas estetica y sin necesidad de poner el *?nums=*
* Se crea la funcion __say_hi__ la cual recibe el request, name y age, que son pasados por la url.

### 2.3 Creacion de la primera app
* Creamos nuestra primera app con el comando __python manage.py startapp posts__
* Esto nos crea una carpeta posts con varios archivos
* En el archivo __settings.py__ agregamos la local app posts
* En el archvio __apps.py__ de la app posts agregamos el __verbose_name__
* En el archvio __urls.py__ creamos una nueva URL, tambien cambiamos la manera de importar las URLs.
* En el archvio __views.py__ de la app posts, creamos la funcioanlidad para dicha vista.

### 2.4 Introduccion al template system
* Modificamos el archivo __views.py__ de la app posts de manera que ya podamos utilizar el template system
* Eliminamos el httpresponse y ahora usamos render
* En el return agregamos un tercer parametro el cual sera el context que recibira el template, es decir la informacion con la que interectuara
* Creamos la plantilla de django __feed.html__ y le damos el formato con bootstrap para que se vea como el feed de Instagram
* Aqui vemos el uso del template system de django.

### 2.5 Patrones de diseño y Django
* __Patron de diseño__ es una solucion reutilizable a un problema comun.
* __MVC Model View Controller__ es una manera de separar los datos(M), de la presentacion(V), de la logica(C)
* __MTV Model Template View__ el que usa Django
  * El modelo(M) es el que se encarga de definir la estructura de los datos.
  * El template(T) es la logica de presentacion de los datos.
  * La vista(V) se encarga de decidir que template va a presentar esos datos

## 3. Models

### 3.1 La M en el MTV
* En el archivo __models.py__ se creo nuestro primer modelo llamado *User* con sus respectivos campos
* Hacemos las migraciones para que no mande las letras en rojo al correr el runserver
* __migrate__ aplica los cambios en la bd
* __makemigrations__ busca los cambios en nuestros modelos y los refleja en un archivo

### 3.2 El ORM de Django
* Se crea un nuevo campo en el archivo __models.py__
* Esta clase vimos el uso del ORM por consola, para hacer consultar, crear, update, etc en la BD
* Vale la pena leer un poco mas

### 3.3 Extendiendo el modelo de usuario
* Hablo de como crear un superusuario __python manage.py createsuperuser__
* Se agrego la url del admin, que se habia borrado por equivocacion
* Hablo del modelo user que trae por defecto django
* Se elimino el contendio de models.py
* Se eliminaron las migraciones

### 3.4 Implementacion del modelo de usuarios de Platzigram
* Creamos una app nueva __users__
* La registramos en el __settings.py__
* En el archivo __models.py__ 
  * Creamos la clase Profile que extiende de models.Model
  * A continuacion usaremos el [aprroach proxy](https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#extending-the-existing-user-model) para hacer nuestro modelo:
    * Declaramos la variable user, la cual sera una relacion de uno a uno al modelo User que es del cual extendemos y tambien el on_delete el cual nos dira como se comportara al momento de ser borrada esa info.
    * Agreagamos los campos que queramos
* En el archivo __apps.py__ solo agregamos informacion descriptiva y el verbose_name

### 3.5 Explorando el dashboard de administración
* En el archivo __admin.py__ de la app users:
  * Importamos modelo Profile
  * Cremos una clase ProfileAdmin para poder personalizar la manera en que se visualizara en nuestro admin

### 3.6 Dashboard de administracion
* En el archivo __admin.py__ da la app users:
  * Utilizamos fieldsets para poder separar por grupos los datos que mostratemos:
    * Una tupla donde el primer valor es el nombre del grupo (lo que aparece en la barra azul)
    * Como segyundo valor un diccionario con el key fields que tendra como valor una tupla con los campos que queramos mostrar. Ojo si los ponemos uno al lado del otro se mostraran de esa manera y si los ponemos abajo del otro asi se mostraran.
  * Delcaramos una clase __ProfileInLine__
    * Hereda del admin la clase StackedInLine, la cual se va a encargar de guardar el modelo Profile de nuestra app users, para luego poder agregarla con el modelo User que trae django por default.
  * Declaramos una clase __UserAdmin__ que hereda de BaseUserAdmin, qu es la clase UserAdmin que django tiene por default en su contrib.auth.admin:
    * Llamamos a inlines y le pasamos la clase __ProfileInLine__
    * Creamos la lista a desplegar con un list_display
  * Por ultimo quitamos el registro del modelo User (django)
  * Regristramos un nuevo modelo, que como base tendra a User y se le agregara el modelo UserAdmin

### 3.7 Creación del modelo de posts
* En el archivo __models.py__ de la app posts
  * Creamos el modelo Post con sus respectivos campos.
* En el archivo __urls.py__
  * Agregamos un hack para que se puedan ver las fotos y los archivos media
* En el archivo __settings.py__
  * Declaramos las variables 
    * __MEDIA_ROOT__ la ruta absoluta del directorio y el nombre de la carpeta donde se almacenaran los archivos, por convencion es *media*
    * __MEDIA_URL__ la url donde estaran los archivos servidos por MEDIA_ROOT, por convencion es */media/*
* En el archivo __admin.py__
  * Registramos el modelo Post
* Hacemos migraciones(makemigration, migrate)

## 4. Templates, auth y middlewares

### 4.1 Templates y archivos estaticos
* Se crean los archivos css y html necesarios para los templates.
* En el archivo __settings.py__se agrega el *STATICFILES_DIRS* Y *STATICFILES_FINDERS (sirve para buscar la carpeta static en todo el proyecto)* asi tambien agregamos la ruta de la carpeta *templates*
* En el archivo __views.py__ de la app post cambiamos la ruta del archivo feed.html, ya que dicho archivo fue movido a la carpeta templates/posts/
* Se explico el uso de los templates y de como los static se utilizan con *load static*

### 4.2 Login
* En el archivo __urls.py__
  * Utilizamos el atributo *name* para un manejo mas sencillo de nuestras urls
  * Creamos una nueva url *users/login* para poder loguearnos
* Creamos el template __login.html__ el cual hereda del otro template *users/base.html*
  * En el *block container* creamos una condicion por si existe algun error.
  * Declaramos un form para poder mandar los datos por el metodo POST y le agregamos el atributo *action* el cual mediante un decorador de *url* le pasamos la ruta del login
  * Utilizamos el *csfr_token* el cual sirve para evitar inputs de otras paginas
* En el archivo __views.py__ de la app posts
  * Utilizamos el decorador *@login_required* que sirve para pedir un login exitoso para poder pasar a ese funcion, que en este caso devuleve el feed.html
* En el archivo __settings.py__
  * Declaramos la varible __LOGIN_URL__ que contien la url de la pagina de login.
* En el archivo __views.py__ de la app users
  * Declaramos la funcion __login_view__ la cual contiene toda la logica si hace login o no

### 4.3 Logout
* En el archivo __urls.py__ creamos una nueva url para el logout
* En el archivo __views.py__ de la app users
  * Creamos una nueva funcion llamada __logout_view__ a la cual se accede solo si esta logueado y esta nos hace logout y nos redirije a la url *login*
* Hicimos algunos cambios en los template *nav y login* 

### 4.4 Signup
* En el archivo __urls.py__ creamos la url para poder hacer signup
* En el archivo __views.py__ de la app users:
  * Creamos la funcion para la vista __signup_view__
  * Recibimos datos del template por el request si el metodo es POST
  * Declaramos un *if* en caso de que las contraseñas no coincidan
  * Usamos un *try y except IntegrityError* en el caso que al crear un usuario el username ya este ocupado
  * Ya que pasamos las pruebas, guardamos el usuario en la bd
  * Asi tambien lo guardamos en el Profile
  * Redireccionamos al login
* Creamos el template *signup.html*

### 4.5 Middlewares
* En el archivo __urls.py__ creamos una url que sera la que nos permita hacer cambios a un profile
* En el archivo __views.py__ de la app users:
  * Creamos una funcion que sera la vista __update_profile__ la cual rendeara el template *update_profile.html*
* Creamos el archivo __middleware.py__
  * Creamos la clase *ProfileCompletionMiddleware*.
  * Declaramos su metodo constructor *__ init __* e inicializamos el get_response
  * Declaramos el metodo *__ call __* en el cual tendremos la logica de el middleware
* En el archivo __settings.py__
  * Agregamos nuestro middleware a la lista MIDDLEWARE
* Agregamos el template __update_profile.html__

## 5. Forms

### 5.1 Formularios en Django
* Creamos el archivo __forms.py__ en la app users
  * Importamos forms de django
  * Creamos la clase *ProfileForm* que hereda de *forms.Form*
  * Declaramos los campos de nuestro form con sus atributos
* Actualizamos el archivo __update_profile.html__ para que pueda mostrar el formulario y sus campos
* En el archivo __views.py__ modificamos la funcion __update_profile__ de tal manera que si el metodo para hacer el update es por POST, se implementa la logica para que los nuevos cambios sean guardados en la bd.

### 5.2 Mostrando el form en el template
* En el archivo __update_profile.html__ mdoificamos algunas lineas, para que las validaciones sean hechas por el django(backend).

### 5.3 Model forms
* En el archivo __urls.py__ creamos una nueva url para poder agregar un post nuevo
* Creamos un archivo __forms.py__ en la app de posts:
  * Contiene una clase de nombre *PostForm* 
  * Tiene una clase *Meta* en la cual le pasaremos el modelo al cual apuntara este formulario
  * Los campos que se utilizaran del modelo referenciado
* En el archivo __views.py__ de la app posts:
  * Modificamos la funcion *list_post* la cual ahora mostrara los post del usuario y quitamos los que estaban hardcodeados
  * Creamos la funcion *create_post* en la cual pasamos la logica para crear un nuevo post.
* Se modician los archivos nav.html y feed.html para que pinten bien los datos
* Se crea un nuevo archivo html new.html el cual es el template para cuando vayamos a crear un nuevo post.

### 5.4 Validacion de formularios
* Aqui lo que vimos es como validar formularios por el backend
* En el archivo __views.py__ de la app users:
  * Refactorizamos la funcion *signup_view*
  * Creamos la variable form que es igual al form *SignupForm* al cual le pasamos el request.POST
  * Si el form es valido lo guarda y de no ser asi, la variable form sera igual al formulario *SignupForm* pero sin datos
  * Retornamos el render pasamoscomo parametros el request, template_name y de contexto el form
* En el archivo __forms.py__ de la app users 
  * Refactorizamos la clase *SignupForm*
  * Aqui vamos a pedir los valores de los campos que requiere el form, no es necesario usar todos los del modelo, y aqui mismo hacemos las primeras validaciones como min y max length
  * Utilizamos widget que sirven en el caso del password para que este dato sea secreto y no este a la vista, igual en el email nos sirve para que en este campo lo que se ingrese debe tener el formato de un email.
  * Se crea la funcion *clean_username* la cual toma los datos del username (cleaned_data) y verifica que no exista en la base de datos(username_taken) con el metodo exists.
  * Creamos la funcion *clean* de inicio almacenos la data antes de sobrescribir la funcion(super), esta funcion django la ejecuta pero aqui la sobreescribiremos
  * Comparamos que password y password_confirmation sean iguales, de no ser asi mandara un mensaje de error
  * Creamos la funcion *save* en la cual obtenemos la data del formulario, sacamos el password_confirmation ya que este no se almacenara en la bd.
  * Cremos el usuario en la bd y para no pasar campo por campo, usamos los kwargs de python(**data)
  * Creamos la variable profile que referencia la modelo Profule y le pasamos el user
  * Guardamos el profile
* Agregamos la opcion de signup en el template __login.html__
* En el template __signup.html__ modificamos la estrucutura de este y en lugar de poner campo por campo, usamos la manera de mostrar los forms de django con *form.as_p*
