"""Platzigram middleware catalog"""

# Django
from django.shortcuts import redirect
from django.urls import reverse

class ProfileCompletionMiddleware:
  """Profile completion middleware

  Ensure every user that is interacting with the platform
  have their profile picture and biography
  """

  def __init__(self, get_response):
    """Middleware initialization"""
    self.get_response = get_response

  def __call__(self, request):
    """Code to be executed for each request before the view is called"""
    
    # Si el usuario no es anonimo, creamos la variable profile la cual
    # sera igual al user.profile que viene por el request
    if not request.user.is_anonymous:

      # Si el usuario es superuser que pase sin problemas, ya que el usuario chars
      # no tiene profile, pero es superuser y devolvemos el get_response
      if request.user.is_superuser:
        return self.get_response(request)

      profile = request.user.profile

      # Si el profile no tiene picture o biography
      if not profile.picture or not profile.biography:        
        # Si la url que viene por el request no es igual a la de update_profile o logout redirecciona al template update_profile
        if request.path not in [reverse('update_profile'), reverse('logout')]:
          return redirect('update_profile')

    # retornamos el get_response
    response = self.get_response(request)
    return response