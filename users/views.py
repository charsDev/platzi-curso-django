"""User views"""

# Django
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

# Forms
from users.forms import ProfileForm, SignupForm

@login_required
def update_profile(request):
  """Update a user's profile view"""

  # Almacenamos el profile que viene por el request.user
  profile = request.user.profile

  # Si el metodo que viene por el request es igual a POST
  if request.method == 'POST':
    # Instanciamos el ProfileForm(forms.py) y le pasamos como parametros el
    # la informacion del formulario que viene por POST y los archivos(FILES)
    # para el caso de las imagenes. 
    form = ProfileForm(request.POST, request.FILES)
    # Si el formulario es valido, limpiamos en pantalla el formulario
    if form.is_valid():
      data = form.cleaned_data
      # pasamos los datos al profile
      profile.website = data['website']
      profile.phone_number = data['phone_number']
      profile.biography = data['biography']
      profile.picture = data['picture']
      # salvamos el profile
      profile.save()
      # redirijimos a la vista update_profile
      return redirect('update_profile')
  else:
    # si no es por POST solo mostramos el formulario
    form = ProfileForm()

  return render(
    request = request, 
    template_name = 'users/update_profile.html',
    # pasamos contexto al template
    context = {
      'profile': profile,
      'user': request.user,
      'form': form
    }
  )

def login_view(request):
  """Login view"""
  if request.method == 'POST':
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user:
      login(request, user)
      return redirect('feed')
    else:
      return render(request, 'users/login.html', {'error': 'Invalid username and password'})

  return render(request, 'users/login.html')

def signup_view(request):
  """Signup view"""
  if request.method == 'POST':
     form = SignupForm(request.POST)
     if form.is_valid():
       form.save()
       return redirect('login')
  else:
    form = SignupForm()

  return render(
    request=request,
    template_name='users/signup.html',
    context={'form': form}
  )

@login_required
def logout_view(request):
  logout(request)
  return redirect('login')