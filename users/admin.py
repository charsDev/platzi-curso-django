"""User admin classes."""

# Django
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib import admin

# Models
from django.contrib.auth.models import User
from users.models import Profile

# Register your models here.
# admin.site.register(Profile) < -- esta es la manera sencilla

# Regsitrar modelo mediante un decorador, cambiar la interfaz y usar filtros
@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
  """Profile admin."""

  # Interfaz de tabla
  list_display = ('pk', 'user', 'phone_number', 'website', 'picture')
  list_display_links = ('pk', 'user')
  list_editable =  ('phone_number', 'website', 'picture')

  # Filtro por caja de busqueda
  search_fields  = (
    'user__email',
    'user__username',
    'user__first_name',
    'user__last_name',
    'phone_number'
  )

  # Filtro por menu. El que aparece a la derecha
  list_filter =  (
    'created',
    'modified',
    'user__is_active',
    'user__is_staff'
  )

  # Separar por grupos como en el modelo Users que trae django por default
  fieldsets = (
    ('Profile', {
      'fields': (
        ('user', 'picture'),
      )
    }),
    ('Extra info', {
      'fields': (
        ('website', 'phone_number'),
        ('biography')
      )
    }),
    ('Metadata', {
      'fields': (
        ('created', 'modified'),
      )
    })
  )

  # Debio a que created y modified son campos que no permiten edicion, de esta forma
  # especificamos que solo seran campos de lectura.
  readonly_fields = ('created', 'modified')

# StackedInLine nos permite poder almacenar un modelo
# para luego poder agregarlo a otro modelo
class ProfileInLine(admin.StackedInline):
  """Profile in-line admin for users."""

  model = Profile
  can_delete = False
  verbose_name_plural = 'profiles'

# Con este clase heredamos de UserAdmin(BaseUserAdmin), le pasamos el modelo
# almacenado en la clase ProfileInLine, declaramos un list_diplay que mostrara los 
# campos delcarados al momento de ver la lista de usuarios
class UserAdmin(BaseUserAdmin):
  """Add profile admin to base admin."""

  inlines = (ProfileInLine,)
  list_display = (
    'username', 
    'email',
    'first_name',
    'last_name',
    'is_active',
    'is_staff',
  )

# Eliminamos registro de User y
# registramos User que agregara lo que este 
# en el UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)

