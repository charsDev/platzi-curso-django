"""Posts admin classes."""

#Django
from django.contrib import admin

#Models
from posts.models import Post

# Register your models here.
@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
  """Post adming."""

  list_display = ('user', 'title', 'photo')
  list_display_links = ('user',)
  list_editable = ('title', 'photo')

  search_fields = (
    'user',
    'title'
  )

  list_filter = (
    'created',
    'modified',
  )